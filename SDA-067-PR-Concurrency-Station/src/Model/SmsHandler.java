package Model;
import java.util.Observable;

public class SmsHandler extends Observable implements Runnable {
	private Sms sms;
	private INotifiable notifiable;
	
	public SmsHandler(Sms sms, INotifiable notifiable) {
		super();
		this.sms = sms;
		this.notifiable = notifiable;
	}

	public Sms getSms() {
		return sms;
	}

	public void setSms(Sms sms) {
		this.sms = sms;
	}

	@Override
	public void run() {
		
		long sleepTime = sms.getMessage().length() * 100;
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setChanged();
		notifyObservers(notifiable);
	}

}

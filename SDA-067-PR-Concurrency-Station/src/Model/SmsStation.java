package Model;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmsStation implements INotifiable {
	private List<Phone> listOfPhones = new LinkedList<>();
	private ExecutorService executors = Executors.newFixedThreadPool(3);
	
	public void addPhone(String phoneNumber){
		Phone phone = new Phone(phoneNumber);
		listOfPhones.add(phone);
	}
	
	public void sendSms(String phoneNumber, String message){
		Sms sms = new Sms(phoneNumber, message);
		SmsHandler handler = new SmsHandler(sms, new INotifiable() {
			
			@Override
			public void receiveNotify() {
				System.out.println("ee makarena");
			}
		});
		
		for(Phone phone: listOfPhones){
			handler.addObserver(phone);
		}
		
		executors.submit(handler);
	}
	
	@Override
	public void receiveNotify() {
		System.out.println("Sms doręczony");
	}
}

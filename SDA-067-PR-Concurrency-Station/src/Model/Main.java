package Model;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		SmsStation ss = new SmsStation();
		
		ss.addPhone("123");
		ss.addPhone("1234");
		ss.addPhone("12345");
		ss.addPhone("12345");
				
		Scanner sc = new Scanner(System.in);
		
		while(sc.hasNextLine()){
			String line = sc.nextLine();
			String[] splits = line.split(" ");
			ss.sendSms((splits[0]), splits[1]);
		}

	}

}

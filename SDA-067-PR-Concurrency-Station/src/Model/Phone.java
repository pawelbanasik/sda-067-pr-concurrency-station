package Model;
import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {
	private String numberPhone;
	
	public Phone(String numberPhone) {
		super();
		this.numberPhone = numberPhone;
	}

	public String getNumberPhone() {
		return numberPhone;
	}
	
	public void setNumberPhone(String numberPhone) {
		this.numberPhone = numberPhone;
	}

	@Override
	public void update(Observable handler, Object notifiable) {
		if(handler instanceof SmsHandler){
			SmsHandler smsHandler = (SmsHandler) handler;
		
			
			Sms sms = smsHandler.getSms();
			if(numberPhone.equals(sms.getPhoneNumber())){
				System.out.println("Otrzymałeś smsa");
				if(notifiable instanceof INotifiable){
					INotifiable not = (INotifiable) notifiable;
					not.receiveNotify();
				}	
			}
		}
	}
}

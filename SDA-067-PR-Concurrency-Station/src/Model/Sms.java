package Model;

public class Sms {
	
	private String message;
	private String phoneNumber;
	
	
	public Sms(String phoneNumber, String message) {
		super();
		this.message = message;
		this.phoneNumber = phoneNumber;
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	

}
